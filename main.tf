provider "aws" {
    region = "me-south-1"
} 

variable "subnet_cidr_block" {
    description = "subnet_cidr_block"
  
}

variable "vpc_cidr_block" {
    description = "vpc_cidr_block"
}

resource "aws_vpc" "development-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
      "Name" = "development"
    }
}


data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "me-south-1a" 
    tags = {
      "Name" = "subnet-2-default"
    }
}   